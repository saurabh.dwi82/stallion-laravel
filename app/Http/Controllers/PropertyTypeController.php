<?php

namespace App\Http\Controllers;

use App\Models\PropertyType;

class PropertyTypeController extends Controller
{
    public function index()
    {
        return response()->json(PropertyType::select('id', 'title')->get());
    }
}
