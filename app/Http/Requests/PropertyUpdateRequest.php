<?php

namespace App\Http\Requests;

class PropertyUpdateRequest extends PropertyCreateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'uuid' => ['required', 'exists:properties,uuid'],
            'image_full' => 'sometimes|nullable|image|mimes:jpeg,png,jpg|max: 512',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge(['uuid' => $this->uuid]);
    }
}
