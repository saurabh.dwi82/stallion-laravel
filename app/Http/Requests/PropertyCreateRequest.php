<?php

namespace App\Http\Requests;

use App\Enums\PropertyTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_type_id' => 'required|exists:property_types,id',
            'county' => 'required|string',
            'country' => 'required|string',
            'town' => 'required|string',
            'description' => 'required|string',
            'post_code' => 'required|string',
            'address' => 'required|string',
            'image_full' => 'required|image|mimes:jpeg,png,jpg|max: 512',
            'num_bedrooms' => 'required|integer|min:1',
            'num_bathrooms' => 'required|integer|min:1',
            'price' => 'required|numeric',
            'type' => ['required', Rule::in([PropertyTypeEnum::Sale->value, PropertyTypeEnum::Rent->value])],
        ];
    }
}
