<?php

namespace App\Console\Commands;

use App\Jobs\ImportPropertyJob;
use Illuminate\Console\Command;

class ImportProperty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import property from api to db';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ImportPropertyJob::dispatch();
    }
}
