<?php

namespace App\Models;

use Database\Factories\PropertyTypeFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description'];

    protected static function newFactory(): Factory
    {
        //App\Models\PropertyType::factory()->count(3)->create();
        return PropertyTypeFactory::new();
    }
}
