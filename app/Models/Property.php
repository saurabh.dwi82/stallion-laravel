<?php

namespace App\Models;

use App\Enums\PropertyTypeEnum;
use Database\Factories\PropertyFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Property extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $casts = [
        'status' => PropertyTypeEnum::class,
    ];

    protected static function newFactory(): Factory
    {
        //App\Models\Property::factory()->count(3)->create();
        return PropertyFactory::new();
    }

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['uuid', 'property_type_id', 'county', 'country', 'town', 'description', 'address', 'image_full', 'image_thumbnail',
        'post_code', 'num_bedrooms', 'num_bathrooms', 'price', 'type'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid());
        });
    }

    public function property_type()
    {
        return $this->hasOne(PropertyType::class, 'id', 'property_type_id');
    }
}
