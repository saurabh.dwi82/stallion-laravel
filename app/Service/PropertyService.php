<?php

namespace App\Service;

use App\Models\Property;

class PropertyService
{
    /**
     * Create property
     */
    public function create(array $request): Property
    {
        return Property::create($this->setRequestData($request));
    }

    /**
     * Return data
     */
    public function show(string $id): Property
    {
        return Property::find($id);
    }

    /**
     * Update property data
     */
    public function update(array $request): int
    {
        return Property::where('uuid', $request['uuid'])->update($this->setRequestData($request));
    }

    /**
     * Delete property from db
     */
    public function delete(string $id): void
    {
        Property::where('uuid', $id)->delete();
    }

    /**
     * Set property data for insert
     */
    public function setRequestData(array $request): array
    {
        $data = [
            'property_type_id' => $request['property_type_id'],
            'county' => $request['county'],
            'country' => $request['country'],
            'town' => $request['town'],
            'description' => $request['description'],
            'address' => $request['address'],
            'num_bedrooms' => $request['num_bedrooms'],
            'num_bathrooms' => $request['num_bathrooms'],
            'price' => $request['price'],
            'type' => $request['type'],
            'post_code' => $request['post_code'],
        ];

        if (isset($request['image_full']) && !empty($request['image_full'])) {
            $data['image_thumbnail'] = $data['image_full'] = (new FileUploadService())->upload($request['image_full'], 'property');
        }

        return $data;
    }
}
