<?php

namespace App\Service;

use Illuminate\Support\Facades\Storage;

class FileUploadService
{
    public function upload($file, $uploadFilePath = '', $deleteFilePath = '')
    {
        if (empty($uploadFilePath)) {
            return null;
        }

        if (! empty($deleteFilePath)) {
            if (Storage::exists($deleteFilePath)) {
                Storage::delete($deleteFilePath);
            }
        }

        return Storage::put($uploadFilePath, $file);
    }
}
