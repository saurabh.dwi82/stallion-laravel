<?php

namespace App\Service;

use App\Jobs\ImportPropertyJob;
use App\Models\Property;
use App\Models\PropertyType;
use Illuminate\Support\Facades\Http;

class PropertyImportService
{
    public function getApiUrl(): string
    {
        return config('services.property.sync_url').'/api/properties?api_key='.config('services.property.api_key');
    }

    /**
     * Function will sync the data from api
     */
    public function sync(string $url = null): void
    {
        $dataUrl = ($url) ? $url : $this->getApiUrl();
        $response = Http::get($dataUrl);

        if ($response->successful()) {
            $this->processData($response->json());
            //$this->runJobForNextData($response->json());
        }
    }

    /**
     * Function will process the data
     */
    public function processData(array $dataToProcess): void
    {
        foreach ($dataToProcess['data'] as $key => $data) {
            $propertyType = $this->propertyType($data['property_type']);
            $this->createProperty($data, $propertyType);
        }
    }

    /**
     * Create property type from api sync
     */
    private function propertyType(array $propertyType): PropertyType
    {
        return PropertyType::firstOrCreate(
            [
                'title' => $propertyType['title'],
            ],
            [
                'title' => $propertyType['title'],
                'description' => $propertyType['description'],
            ]
        );
    }

    /**
     * Create property on api sync
     */
    private function createProperty(array $property, PropertyType $propertyType): Property
    {
        return Property::create(
            [
                'uuid' => $property['uuid'],
                'property_type_id' => $propertyType->id,
                'county' => $property['county'],
                'country' => $property['country'],
                'town' => $property['town'],
                'description' => $property['description'],
                'address' => $property['address'],
                'image_full' => $property['image_full'],
                'image_thumbnail' => $property['image_thumbnail'],
                'latitude' => $property['latitude'],
                'longitude' => $property['longitude'],
                'num_bedrooms' => $property['num_bedrooms'],
                'num_bathrooms' => $property['num_bathrooms'],
                'price' => $property['price'],
                'type' => $property['type'],
            ]
        );
    }

    /**
     * If any next data is available then run job to sync more data
     */
    public function runJobForNextData(array $dataToProcess): void
    {
        if ($dataToProcess['next_page_url']) {
            ImportPropertyJob::dispatch($dataToProcess['next_page_url']);
        }
    }
}
