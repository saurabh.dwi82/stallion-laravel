<?php

namespace App\Enums;

enum PropertyTypeEnum: string
{
    case Sale = 'sale';
    case Rent = 'rent';
}
