<?php

namespace App\Jobs;

use App\Service\PropertyImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportPropertyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $url = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $url = null)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PropertyImportService $pSobj)
    {
        $pSobj->sync($this->url);
    }
}
