<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->foreignId('property_type_id')->constrained();
            $table->string('county');
            $table->string('country');
            $table->string('town');
            $table->longText('description');
            $table->string('address');
            $table->string('image_full');
            $table->string('image_thumbnail');
            $table->string('post_code')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->smallInteger('num_bedrooms');
            $table->smallInteger('num_bathrooms');
            $table->decimal('price', $precision = 10, $scale = 2);
            $table->string('type');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
