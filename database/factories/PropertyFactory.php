<?php

namespace Database\Factories;

use App\Models\PropertyType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class PropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->uuid(),
            'property_type_id' => PropertyType::factory()->create()->id,
            'county' => fake()->country(),
            'country' => fake()->country(),
            'town' => fake()->city(),
            'description' => fake()->text(500),
            'address' => fake()->streetAddress(),
            'image_full' => fake()->imageUrl(),
            'image_thumbnail' => fake()->imageUrl(100, 100),
            'post_code' => fake()->postcode(),
            'latitude' => fake()->latitude(),
            'longitude' => fake()->longitude(),
            'num_bedrooms' => fake()->numberBetween(1, 5),
            'num_bathrooms' => fake()->numberBetween(1, 3),
            'price' => fake()->numberBetween(100000, 1000000),
            'type' => fake()->randomElement(['sale', 'rent']),
        ];
    }
}
