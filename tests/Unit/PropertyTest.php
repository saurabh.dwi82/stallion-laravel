<?php

namespace Tests\Unit;

use App\Models\Property;
use App\Service\PropertyService;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    public $propertyService;

    public function setup(): void
    {
        parent::setup();

        $this->propertyService = new PropertyService();
    }

    /**
     * A basic test example.
     */
    public function test_create(): void
    {
        $property = Property::factory()->create();
        $this->propertyService->create($property->toArray());
        $this->assertDatabaseHas('properties', [
            'uuid' => $property->uuid,
        ]);
    }

    public function test_delete(): void
    {
        $property = Property::factory()->create();
        $this->propertyService->delete($property->uuid);
        $this->assertSoftDeleted('properties', [
            'uuid' => $property->uuid,
        ]);
    }

    public function test_show(): void
    {
        $property = Property::factory()->create();
        $db = $this->propertyService->show($property->uuid);
        $this->assertEquals($property->uuid, $db->uuid);
        $this->assertEquals($property->property_type_id, $db->property_type_id);
        $this->assertEquals($property->town, $db->town);
        $this->assertEquals($property->post_code, $db->post_code);
    }
}
