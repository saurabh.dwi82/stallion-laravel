<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Property;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_the_application_returns_a_successful_response(): void
    {
        Property::factory()->create();
        $response = $this->get('api/properties');
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uuid',
                        'property_type_id',
                        'county',
                        'country',
                        'town',
                        'description',
                        'address',
                        'image_full',
                        'image_thumbnail',
                        'post_code',
                        'latitude',
                        'longitude',
                        'num_bedrooms',
                        'num_bathrooms',
                        'price',
                        'type',
                        'property_type' => [
                            'id',
                            'title',
                            'description',
                            'created_at',
                            'updated_at',
                        ],

                    ],
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'path',
                    'per_page',
                    'to',
                ],
            ]);

    }

    public function test_get_property_by_id(): void
    {
        $property = Property::factory()->create();
        $response = $this->get('api/properties/'.$property->uuid);
        $response->assertStatus(200)
            ->assertJson([
                'data' => $property->toArray(),
            ]);
    }

    public function test_delete_property(): void
    {
        $property = Property::factory()->create();
        $response = $this->delete('api/properties/'.$property->uuid);
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ])->assertJson([
                'message' => 'Property deleted successfully.',
            ]
            );
    }
}
