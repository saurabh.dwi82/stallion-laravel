<?php

return [

    'sync' => [
        'completed' => 'Property sync has been completed successfully.',

    ],
    'added' => 'Property added successfully.',
    'updated' => 'Property updated successfully.',
    'deleted' => 'Property deleted successfully.',

];
